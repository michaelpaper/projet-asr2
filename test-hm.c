#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <assert.h>
#include "hm.h"


// CRASH TEST
int main(int argc,char **argv){
  (void) argc;
  (void) argv;

  int i;
  srandom(time(NULL));
  int* t[10];
  int sizes[10];
  for (i=0; i<10; i++)
    t[i] = NULL;
  for (i=0; i<10000; i++)
  {
    int x = random() % 10;
    int s = 1 + (random() % 2000);
    int j;
    if (t[x] == NULL)
    {
      printf("MALLOC x=%d\ts=%d\n", x, s);
      t[x] = malloc(s*sizeof(int));
      sizes[x] = s;
      // to test the integrity of the allocated array
      for (j=0; j<sizes[x]; j++)
	t[x][j] = j;
    }
    else
      if (random()%2)
      {
	printf("REALLOC x=%d\ts=%d\n", x, s);
	for (j=0; j<sizes[x]; j++)
	  if(t[x][j] != j)
	  {
	    fprintf(stderr, "j=%d\tsize=%d\n", j, sizes[x]);
	    exit(1);
	  }
	t[x] = realloc(t[x], s*sizeof(int));
	sizes[x] = s;
	for (j=0; j<sizes[x]; j++)
	  t[x][j] = j;
      }
      else
      {
	printf("FREE x=%d\n", x);
	// testing the integrity
	for (j=0; j<sizes[x]; j++)
	  if(t[x][j] != j)
	  {
	    fprintf(stderr, "j=%d\tsize=%d\n", j, sizes[x]);
	    exit(1);
	  }
	free(t[x]);
	t[x] = NULL;
	sizes[x] = 0;
      }
  }
  printf("On libère tout\n");
  for (i=0; i<10; i++)
    if (t[i] != NULL)
    {
      free(t[i]);
      t[i] = NULL;
    }
}

