#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <pthread.h>
#include <string.h>

#include "hm.h"

struct bloc *cache=NULL;

pthread_mutex_t allocating;
pthread_mutexattr_t Attr;
pthread_mutex_t initializing = PTHREAD_MUTEX_INITIALIZER;


//Un invariant que l'on va conserver est que le dernier bloc est toujours libre (state == 0) : il s'agit de la marge qu'on ira étendre si on a pas pu insérer le bloc avant.

//on va créer un fils libre à ce bloc, en lui allouant toute la taille de ce bloc moins celle nécessaire pour l'allocation (et le bloc "to_split" sera donc utilisé pour l'allocation).
//cela permet de conserver le fait que le dernier bloc est de statut 0 (si on est en train de spliter le dernier bloc)
//On s'arrange ensuite pour recoller les pointeurs, en faisant gaffe au cas où le successeur de ce bloc est NULL
void *split(struct bloc *to_split, int size)
{
  char * aux = (char*)to_split;
  struct bloc *new_bloc = (struct bloc *)(aux + sizeof(struct bloc) + size);
  new_bloc->next = to_split->next;
  to_split->next = new_bloc;
  new_bloc->size_to_malloc = (to_split->size_to_malloc) - size - sizeof(struct bloc);
  to_split->size_to_malloc = size;
  to_split->state = 1;
  new_bloc->state = 0;
  new_bloc->predecessor = to_split;
  if (new_bloc->next != NULL)
    (new_bloc->next)->predecessor = new_bloc;
  return new_bloc;
}

//to_merge a été libéré, et on regarde si son successeur l'est aussi. Le cas échéant, on fusionne.
//on fait alors gaffe de recoller les `next` et `predecessor`
void try_to_merge_with_next(struct bloc *to_merge){
  if (to_merge->next != NULL && (to_merge->next)->state == 0)
    {
      struct bloc* fils = to_merge->next;
      to_merge->next = fils->next;
      to_merge->size_to_malloc += fils->size_to_malloc + sizeof(struct bloc);
      if (fils->next != NULL)
	(fils->next)->predecessor = to_merge;
    }
}

//même chose, mais avec le prédecesseur
void try_to_merge_with_pred(struct bloc *to_merge){
  if (to_merge->predecessor != NULL && (to_merge->predecessor)->state == 0)
    {
      struct bloc* pere = to_merge->predecessor;
      pere->next = to_merge->next;
      pere->size_to_malloc += to_merge->size_to_malloc + sizeof(struct bloc);
      if (to_merge->next != NULL)
	{
	  (to_merge->next)->predecessor = pere;
	}
    }
}

//on crée le premier bloc
//et on initialise le mutex comme récursif (pour `realloc` qui peut appeler `malloc`)
void init_heap(){
  cache=sbrk(0);
  sbrk(INITIAL_HEAP_SIZE);
  cache->next=NULL;
  cache->predecessor = NULL;
  cache->size_to_malloc=INITIAL_HEAP_SIZE-sizeof(struct bloc);
  cache->state=0;
  pthread_mutexattr_init(&Attr);
  pthread_mutexattr_settype(&Attr, PTHREAD_MUTEX_RECURSIVE);
  pthread_mutex_init(&allocating, &Attr);
}
  
//on parcourt la liste des blocs. Tant que le bloc est utilisé (i.e. state == 1) ou trop petit, on passe au suivant.
//Si il n'existe pas de suivant, alors on en est au dernier bloc, et on l'étend alors pour en créer un juste de la bonne taille
//Puis on split le bloc trouvé
extern void *malloc(size_t size)
{
  pthread_mutex_lock(&initializing);
  if(cache==NULL)
    init_heap();
  pthread_mutex_unlock(&initializing);
  pthread_mutex_lock(&allocating);
  struct bloc *regarde = cache;
  while (regarde->state == 1 || (regarde->size_to_malloc < ((int) sizeof(struct bloc))+ size))
    {
      if(regarde->next == NULL)
	{//donc si on est le dernier bloc, l'étendre
	  sbrk(size + sizeof(struct bloc));
	  regarde->size_to_malloc += size+ sizeof(struct bloc);
	}
      else
	regarde = regarde->next;
    }
  split(regarde, size);
  pthread_mutex_unlock(&allocating);
  void* result = (void*) regarde;
  result += sizeof(struct bloc);
  return result;
}

//On libère le bloc en mettant `state` à 0, puis on essaie de le fusionner à ses éventuels voisins libres
//si le dernier bloc devient trop grand, pour ne pas retenir trop de mémoire, on fait un sbrk pour le mettre à INITIAL_HEAP_SIZE.
//on est obligés de définir pred avant le try_to_merge_with_pred sinon on en perd complètement trace, puis on traite lequel des deux est éventuellement devenu dernier bloc en faisant le sbrk et mettant à jour sa size_to_malloc
extern void free(void *ptr)
{
  struct bloc *regarde = ptr - sizeof(struct bloc);
  pthread_mutex_lock(&allocating);
  regarde->state = 0;
  try_to_merge_with_next(regarde);
  struct bloc *pred = regarde->predecessor;
  try_to_merge_with_pred(regarde);
  if (pred->next == NULL && pred->size_to_malloc > INITIAL_HEAP_SIZE)
    {
      sbrk(INITIAL_HEAP_SIZE - pred->size_to_malloc);
      pred->size_to_malloc = INITIAL_HEAP_SIZE;
    }
  else if (regarde->next == NULL && regarde->size_to_malloc > INITIAL_HEAP_SIZE)
    {
      sbrk(INITIAL_HEAP_SIZE - regarde->size_to_malloc);
      regarde->size_to_malloc = INITIAL_HEAP_SIZE;
    }
  pthread_mutex_unlock(&allocating);
}

//on vérifie d'abord si le pointeur est nul : dans ce cas on fait juste un malloc
//sinon, et si la nouvelle taille est plus grande que la taille actuelle : on essaie d'abord de fusionner avec son éventuel fils libre
//puis, si ça n'a pas suffi, on fait un malloc, on copie les données, puis on free l'ancien bloc
//Si la nouvelle taille est plus petite, soit on a la taille pour caser un nouveau bloc dans l'espace libéré, auquel cas on split pour le faire
//sinon, on laisse alors le bloc en l'état, car on n'est pas en mesure d'utiliser la place que l'on libérerait alors
extern void *realloc(void *ptr, size_t size){
  pthread_mutex_lock(&allocating);
  if (ptr==NULL)
    {
      return malloc(size);
    }
  struct bloc *ancien = ((void*) ptr) - sizeof(struct bloc);
  size_t old_size = ancien->size_to_malloc;
  void* result;
  if (size>old_size)
    {
      if (ancien->next->next != NULL)
	try_to_merge_with_next(ancien);
      size_t new_size = ancien->size_to_malloc;
      if (new_size<size)
	{
	  result = malloc(size);
	  memcpy(result, ptr, old_size);
	  free(ptr);
	}
      else
	result = ptr;
    }
  else if (size==0)
    {
      free(ptr);
      result = NULL;
    }
  else if (size > (int) sizeof (struct bloc) + old_size)
    {
      split(ancien, size);
      try_to_merge_with_next(ancien->next);
      result = ptr;
    }
  else
    result = ptr;
  pthread_mutex_unlock(&allocating);
  return result;
}
