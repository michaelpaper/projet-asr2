#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <signal.h>

#define FILEPATH "/tmp/shared_mem.bin"
#define FILESIZE 4096

unsigned int quanta = 500;
unsigned int number_of_vcpus = 0;
unsigned int number_of_active_uthreads = 0;
int *map;  /* mmapped array of int's */
int numero = 0;

void transfere_uthread();

void recoit_uthread();

void set_quanta(unsigned int new_quanta)
{
  struct itimerval it_val;
  quanta = new_quanta;
  it_val.it_value.tv_sec = new_quanta/1000;
  it_val.it_value.tv_usec = (new_quanta*1000) % 1000000;
  it_val.it_interval = it_val.it_value;
  setitimer(ITIMER_REAL, &it_val, NULL);
}

void actualise_compte()
{
  int i = 3*numero;
  map[i+1] = number_of_vcpus;
  map[i+2] = number_of_active_uthreads;
  if (map[6+numero]==1)
    {//1 signifie qu'on nous demande un uthread
      transfere_uthread;
      map[6+numero] = 2;//2 signifie qu'on a donné un uthread
      printf("uthread donné\n");
    }
  else if (map[6+numero] == 3)
    {//3 signifie qu'on a reçu un uthread
      recoit_uthread;
      map[6+numero] = 0;
      printf("uthread reçu\n");
    }
}
  

int main(/*int numero*/)
{
  int i; //l'adresse de nos deux entiers de controle
  int i2; //l'adresse des deux entiers de controle de l'autre VM
  int fd;
  int result;

    
  /* Open a file for writing.
   *  - Creating the file if it doesn't exist.
   *  - Truncating it to 0 size if it already exists. (not really needed)
   *
   * Note: "O_WRONLY" mode is not sufficient when mmaping.
   */
  fd = open(FILEPATH, O_RDWR | O_CREAT | O_SYNC, (mode_t)0700);
  if (fd == -1) {
    perror("Error opening file for writing");
    exit(EXIT_FAILURE);
  }
  
  result = lseek(fd, FILESIZE-1, SEEK_SET);
  if (result == -1) {
    close(fd);
    perror("Error calling lseek() to 'stretch' the file");
    exit(EXIT_FAILURE);
  }
  //puis a priori il faut écrire un bit à la fin du fichier pour s'assurer que le fait de l'avoir étendu ait marché
  result = write(fd, "", 1);
  if (result != 1) {
    close(fd);
    printf("erreur d'écriture du dernier bit\n");
    perror("Error writing last byte of the file");
    exit(EXIT_FAILURE);
  }
    
  map = mmap(0, FILESIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (map == MAP_FAILED) {
    close(fd);
    perror("Error mmapping the file");
    exit(EXIT_FAILURE);
  }
    
  //i représente le début des deux int réservés pour ce processus, où il pourra écrire son état
  i = 3*numero;
  i2 = 3-i;
  map[i] = 100; //100 = j'ai bien démarré
  printf("démarrage normal du processus\n");
  
  while (map[i2] < 100)
    {
      sleep(1);
    }
  map[i] = 200; //j'ai démarré et vu l'autre

  struct sigaction sa_alarm;
  set_quanta(quanta);
  // Initialisation des signaux
  sa_alarm.sa_handler = actualise_compte; // SIGALRM
  sa_alarm.sa_flags = 0;
  sigemptyset(&sa_alarm.sa_mask);
  sigaddset(&sa_alarm.sa_mask, SIGALRM);
  sigaddset(&sa_alarm.sa_mask, SIGALRM);
  sigaction(SIGALRM, &sa_alarm, NULL);
  printf("j'ai vu l'autre processus et l'alarme est initialisée\n");

  while(1)
    {
      sleep(1);
    }
    
  if (msync((void *) map, FILESIZE, MS_SYNC) == -1){
    perror("error in synchronizing the file before unmapping it");
  }
  
  if (munmap(map, FILESIZE) == -1) {
    perror("Error un-mmapping the file");
  }
  
  printf("fermeture standard du processus\n");
  close(fd);
  return 0;
}
