#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h> //a retirer, juste pour les tests

#include "uthreads.h"

uthread_t uthreads[16];

void* uthread_func(void* arg)
{
  int* num = (int*) arg;
  int i, j, k, l;
  uthread_kill(uthreads[(*num+1)%16], SIGCHLD);
  for (i=0; i<10; i++)
  {
    printf("Je suis le uthread %d à l'itération %d !\n", *num, i);
    for (j=0; j<(*num<8 ? 3 : *num < 15 ? 6 : 19) * 10000; j++)
      for (k=0; k<1000; k++)
	l++;
  }
  printf("\e[1;32mJe rends la main en tant que uthread %d\e[0m\n", *num);
  uthread_exit(NULL);
  return NULL;
}

int main()
{
  sleep(1);
  int i;
  if (init_vcpus(3, 1) == -1)
    exit(EXIT_FAILURE);
  int* args = malloc(16*sizeof(int));
  for (i=0; i<16; i++)
    args[i] = i;
  for (i=0; i<16; i++)
    uthread_create(&uthreads[i], uthread_func, &args[i]);
  for (i=0; i < 8; i++)
    uthread_join(uthreads[i], NULL);
  return 0;
}
