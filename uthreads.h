#ifndef __UTHREADS_H__
#define __UTHREADS_H__

#include <ucontext.h>
#include <pthread.h>

#define MAX_VCPUS 32
#define MAX_UTHREADS 4096
#define STACK_SIZE (1 << 21)
#define MAX_SIG 32

#define VCPU_ALIVE             0
#define VCPU_DYING             1
#define VCPU_DEAD              2
#define VCPU_NOT_INITIALIZED   3

typedef struct _thread_file _thread_file;

struct _thread_file{
  int value;
  _thread_file *successeur;
};

typedef struct uthread {
  ucontext_t ctx;
  int is_dead;
  int is_active;
  int id;
  _thread_file signaux;
  _thread_file *fin_file;
  void* result;
  int vcpu;
  int is_joined;
  int to_be_freed;
  pthread_t destinataire;
} uthread_t;

typedef struct vcpu {
  pthread_t thread;
  uthread_t idle;
  uthread_t* active_process;
  int status; /* utiliser les macros VCPU_* pour ce champ */
} vcpu_t;

// Met en route le scheduler pour `number` vcpus avec l'identifiant `id`
int init_vcpus(unsigned int number, int id);

// Modifie à la volée le quanta du scheduler, en millisecondes
void set_quanta(unsigned int quanta);

// Modifie le nombre de VCPUs (number doit être supérieur à 0)
void set_vcpus(unsigned int number);

// Crée un thread utilisateur et le lance
int uthread_create(uthread_t *thread, void* (*start_routine) (void *), void *arg);

// Envoie un signal à un autre uthread
void uthread_kill(uthread_t thread, int sig);

// Termine un uthread
void uthread_exit(void *retval);

// Rend la main au scheduler
void uthread_yield(void);

// Attend la terminaison d'un uthread
void uthread_join(uthread_t thread, void **retval);

#endif
