#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h> //a retirer, juste pour les tests
#include <time.h>

#include "uthreads.h"

uthread_t uthreads[16];

void* uthread_func(void* arg)
{
  int* num = (int*) arg;
  int i, j;
  int* t[10];
  int sizes[10];
  int x, s;
  uthread_kill(uthreads[(*num+1)%16], SIGCHLD);
  srandom(time(NULL));

  for (i=0; i<10; i++)
    t[i] = NULL;
  for (i=0; i<5000; i++)
  {
    if (i%1000 == 0)
      printf("[uthread %d] Itération %d\n", *num, i);
    x = random() % 10;
    s = 1 + (random() % 2000);
    if (t[x] == NULL)
    {
      // malloc
      t[x] = malloc(s*sizeof(int));
      sizes[x] = s;
      // to test the integrity of the allocated array
      for (j=0; j<sizes[x]; j++)
	t[x][j] = j;
    }
    else
      if (random()%2)
      {
	// testing the integrity
	for (j=0; j<sizes[x]; j++)
	  if (t[x][j] != j)
	  {
	    fprintf(stderr, "Array corrupted\n");
	    exit(EXIT_FAILURE);
	  }
	// realloc
	t[x] = realloc(t[x], s*sizeof(int));
	sizes[x] = s;
	for (j=0; j<sizes[x]; j++)
	  t[x][j] = j;
      }
      else
      {
	// testing the integrity
	for (j=0; j<sizes[x]; j++)
	  if (t[x][j] != j)
	  {
	    fprintf(stderr, "Array corrupted\n");
	    exit(EXIT_FAILURE);
	  }
	// free
	free(t[x]);
	t[x] = NULL;
	sizes[x] = 0;
      }
  }
  for (i=0; i<10; i++)
    if (t[i] != NULL)
    {
      free(t[i]);
      t[i] = NULL;
    }
  printf("\e[1;32mJe rends la main en tant que uthread %d\e[0m\n", *num);
  uthread_exit(NULL);
  return NULL;
}

int main()
{
  int i;
  // Initialisation de 4 vcpus
  if (init_vcpus(4, 0) == -1)
    exit(EXIT_FAILURE);
  // Initialisation de 16 uthreads
  int* args = malloc(16*sizeof(int));
  for (i=0; i<16; i++)
    args[i] = i;
  for (i=0; i<16; i++)
    uthread_create(&uthreads[i], uthread_func, &args[i]);
  // On fait de l'attente passive sur les 8 premiers uthreads
  for (i=0; i < 8; i++)
    uthread_join(uthreads[i], NULL);
  // On modifie le quanta
  printf("\e[1;31mmise à jour du quanta\e[0m\n");
  set_quanta(200);
  // On attend les 8 derniers uthreads
  for (i=8; i < 16; i++)
    uthread_join(uthreads[i], NULL);
  printf("fini !\n");
  return 0;
}
